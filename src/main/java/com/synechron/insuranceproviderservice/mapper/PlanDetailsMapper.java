package com.synechron.insuranceproviderservice.mapper;

import java.util.List;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import com.synechron.insuranceproviderservice.dto.PlanDetailsDTO;
import com.synechron.insuranceproviderservice.entity.PlanDetails;

@Mapper(componentModel = "spring", uses = {
		PlanTypeMasterMapper.class }, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanDetailsMapper {

	PlanDetailsDTO convertEntityToDTO(PlanDetails planDetails);

	@InheritConfiguration(name = "convertEntityToDTO")
	PlanDetails convertDTOToEntity(PlanDetailsDTO pDetailsDTO);

	List<PlanDetailsDTO> convertEntityListToDTOList(List<PlanDetails> planDetails);

	@InheritConfiguration(name = "convertEntityListToDTOList")
	List<PlanDetails> convertDTOListToEntityList(List<PlanDetailsDTO> planDetailsDTOs);

}
