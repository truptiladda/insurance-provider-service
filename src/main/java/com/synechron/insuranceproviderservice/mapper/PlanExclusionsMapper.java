package com.synechron.insuranceproviderservice.mapper;

import java.util.List;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;

import com.synechron.insuranceproviderservice.dto.PlanExclusionsDTO;
import com.synechron.insuranceproviderservice.entity.PlanExclusions;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanExclusionsMapper {

	@Mappings({ @Mapping(source = "planId.planId", target = "planId") })
	PlanExclusionsDTO convertEntityToDTO(PlanExclusions planExclusion);

	@Mappings({ @Mapping(source = "planId", target = "planId.planId") })
	PlanExclusions convertDTOToEntity(PlanExclusionsDTO planExclusionsDTO);

	List<PlanExclusionsDTO> convertEntityListToDTOList(List<PlanExclusions> planExclusions);

	@InheritConfiguration(name = "convertEntityListToDTOList")
	List<PlanExclusions> convertDTOListToEntityList(List<PlanExclusionsDTO> planDetailsDTOs);
}
