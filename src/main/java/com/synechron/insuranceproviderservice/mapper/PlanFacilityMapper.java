package com.synechron.insuranceproviderservice.mapper;

import java.util.List;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;

import com.synechron.insuranceproviderservice.dto.PlanFacilitiesDTO;
import com.synechron.insuranceproviderservice.entity.PlanFacilities;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanFacilityMapper {

	@Mappings({@Mapping(source = "planId.planId", target = "planId")})
	PlanFacilitiesDTO convertEntityToDTO(PlanFacilities planFacilities);

//	@InheritConfiguration(name = "convertEntityToDTO")
	@Mappings({@Mapping(source = "planId", target = "planId.planId")})
	PlanFacilities convertDTOToEntity(PlanFacilitiesDTO planFacilitiesDTO);

	List<PlanFacilitiesDTO> convertEntityListToDTOList(List<PlanFacilities> planFacilities);

	@InheritConfiguration(name = "convertEntityListToDTOList")
	List<PlanFacilities> convertDTOListToEntityList(List<PlanFacilitiesDTO> planFacilitiesDTOs);
}
