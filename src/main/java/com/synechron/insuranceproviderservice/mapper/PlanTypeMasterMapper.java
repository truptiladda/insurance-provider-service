package com.synechron.insuranceproviderservice.mapper;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import com.synechron.insuranceproviderservice.dto.PlanTypeMasterDTO;
import com.synechron.insuranceproviderservice.entity.PlanTypeMaster;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PlanTypeMasterMapper {

	PlanTypeMaster convertDTOToEntity(PlanTypeMasterDTO planTypMasterDTO);

	@InheritConfiguration(name = "convertDTOToEntity")
	PlanTypeMasterDTO convertEntityToDTO(PlanTypeMaster planTypMaster);

}
