package com.synechron.insuranceproviderservice.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PlanExclusionsDTO implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private Integer planExclusionId;

	private String planExclusion;

	private Integer planId;

}
