package com.synechron.insuranceproviderservice.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PlanFacilitiesDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer planFacilityId;

	private String planFacility;

	private Integer planId;
}
