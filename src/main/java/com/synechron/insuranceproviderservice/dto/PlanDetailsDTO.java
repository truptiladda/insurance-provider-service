package com.synechron.insuranceproviderservice.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class PlanDetailsDTO {

	private Integer planId;

	private String planName;

	private Integer entryAge;

	private Integer maturityAge;

	private BigDecimal premiumYearly;

	private Boolean isCopayment;

	private PlanTypeMasterDTO planTypeId;

	private List<PlanFacilitiesDTO> planFacilitiesDTOs;
	
	private List<PlanExclusionsDTO> planExclusionDTOs;

}
