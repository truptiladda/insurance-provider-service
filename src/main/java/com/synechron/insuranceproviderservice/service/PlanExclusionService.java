package com.synechron.insuranceproviderservice.service;

import java.util.List;

import com.synechron.insuranceproviderservice.dto.PlanExclusionsDTO;

public interface PlanExclusionService {

	List<PlanExclusionsDTO> retrieveByPlanId(Integer planId);

	List<PlanExclusionsDTO> saveAll(List<PlanExclusionsDTO> planExclusionsDTOs);

	List<PlanExclusionsDTO> updateAll(List<PlanExclusionsDTO> planExclusionsDTOs);

	void deleteByPlanId(Integer planId);

}
