package com.synechron.insuranceproviderservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.insuranceproviderservice.dto.PlanDetailsDTO;
import com.synechron.insuranceproviderservice.dto.PlanExclusionsDTO;
import com.synechron.insuranceproviderservice.dto.PlanFacilitiesDTO;
import com.synechron.insuranceproviderservice.entity.PlanDetails;
import com.synechron.insuranceproviderservice.mapper.PlanDetailsMapper;
import com.synechron.insuranceproviderservice.repository.PlanDetailsRepository;
import com.synechron.insuranceproviderservice.service.PlanDetailsService;
import com.synechron.insuranceproviderservice.service.PlanExclusionService;
import com.synechron.insuranceproviderservice.service.PlanFacilityService;

@Service
public class PlanDetailsServiceImpl implements PlanDetailsService {

	@Autowired
	private PlanDetailsRepository planDetailsRepository;

	@Autowired
	private PlanDetailsMapper planDetailsMapper;

	@Autowired
	private PlanFacilityService planFacilityService;

	@Autowired
	private PlanExclusionService planExclusionService;

	// save into plan details
	@Override
	public PlanDetailsDTO createPlan(PlanDetailsDTO planDetailsDTO) {
		PlanDetails planDetails = planDetailsMapper.convertDTOToEntity(planDetailsDTO);
		planDetails = planDetailsRepository.save(planDetails);
		return planDetailsMapper.convertEntityToDTO(planDetails);
	}

	// save in plan details, facilities, exclusions
	@Override
	public List<PlanDetailsDTO> createAllPlans(List<PlanDetailsDTO> planDetailsDTOs) {
		List<PlanDetailsDTO> saved = new ArrayList<>();
		planDetailsDTOs.forEach(planDetails -> {
			PlanDetailsDTO planDetailDTO = createPlan(planDetails);
			planDetails.getPlanExclusionDTOs()
					.forEach(planExclusion -> planExclusion.setPlanId(planDetailDTO.getPlanId()));
			planDetails.getPlanFacilitiesDTOs()
					.forEach(planfacilty -> planfacilty.setPlanId(planDetailDTO.getPlanId()));
			List<PlanFacilitiesDTO> facilityDTOs = planFacilityService.saveAll(planDetails.getPlanFacilitiesDTOs());
			List<PlanExclusionsDTO> exclusionsDTOs = planExclusionService.saveAll(planDetails.getPlanExclusionDTOs());
			planDetailDTO.setPlanFacilitiesDTOs(facilityDTOs);
			planDetailDTO.setPlanExclusionDTOs(exclusionsDTOs);
			saved.add(planDetailDTO);

		});
		return saved;
	}

	// update into plan details
	@Override
	public PlanDetailsDTO updatePlan(PlanDetailsDTO planDetailsDTO) {
		PlanDetails toUpdate = new PlanDetails();
		Optional<PlanDetails> details = planDetailsRepository.findById(planDetailsDTO.getPlanId());
		if (details.isPresent()) {
			toUpdate = details.get();
			toUpdate = planDetailsMapper.convertDTOToEntity(planDetailsDTO);
			toUpdate = planDetailsRepository.save(toUpdate);
		}
		return planDetailsMapper.convertEntityToDTO(toUpdate);
	}

	// update in plan details, facilities, exclusions
	@Override
	public List<PlanDetailsDTO> updateAllPlans(List<PlanDetailsDTO> planDetailsDTOs) {
		List<PlanDetailsDTO> updated = new ArrayList<>();
		planDetailsDTOs.forEach(planDetails -> {
			PlanDetailsDTO planDetailDTO = updatePlan(planDetails);
			planDetails.getPlanFacilitiesDTOs().forEach(facility -> facility.setPlanId(planDetails.getPlanId()));
			planDetails.getPlanExclusionDTOs().forEach(exclusion -> exclusion.setPlanId(planDetails.getPlanId()));
			List<PlanFacilitiesDTO> planFacilites = planFacilityService.updateAll(planDetails.getPlanFacilitiesDTOs());
			List<PlanExclusionsDTO> planExclusions = planExclusionService.updateAll(planDetails.getPlanExclusionDTOs());

			planDetailDTO.setPlanExclusionDTOs(planExclusions);
			planDetailDTO.setPlanFacilitiesDTOs(planFacilites);
			updated.add(planDetailDTO);
		});

		return updated;
	}

	@Override
	public List<PlanDetailsDTO> retrieveAllPlans() {
		List<PlanDetails> planDetails = planDetailsRepository.findAll();
		return planDetailsMapper.convertEntityListToDTOList(planDetails);
	}

	// retrieve whole plan
	@Override
	public PlanDetailsDTO retrieveByPlanId(Integer planId) {
		Optional<PlanDetails> planDetails = planDetailsRepository.findById(planId);
		if (planDetails.isPresent()) {
			PlanDetailsDTO planDetailsDTO = planDetailsMapper.convertEntityToDTO(planDetails.get());
			if (planDetailsDTO != null) {
				List<PlanFacilitiesDTO> facilitiesDTOs = planFacilityService.retrieveByPlanId(planId);
				List<PlanExclusionsDTO> planExclusionDTOs = planExclusionService.retrieveByPlanId(planId);
				planDetailsDTO.setPlanExclusionDTOs(planExclusionDTOs);
				planDetailsDTO.setPlanFacilitiesDTOs(facilitiesDTOs);
			}
			return planDetailsDTO;
		}
		return null;
	}

	@Override
	public void deleteByPlanId(Integer planId) {
		PlanDetails details = planDetailsRepository.findById(planId).get();
		if (details != null) {
			planFacilityService.deleteByPlanId(planId);
			planExclusionService.deleteByPlanId(planId);
			planDetailsRepository.deleteById(planId);
		}
	}

	@Override
	public List<PlanDetailsDTO> retrieveByPlanType(Integer planTypeId) {

		List<PlanDetails> planDetails = planDetailsRepository.findByPlanTypeId(planTypeId);
		List<PlanDetailsDTO> planDetailsDTOs = planDetailsMapper.convertEntityListToDTOList(planDetails);
		planDetailsDTOs.forEach(planDetail -> {
			List<PlanFacilitiesDTO> planFacilities = planFacilityService.retrieveByPlanId(planDetail.getPlanId());
			List<PlanExclusionsDTO> exclusions = planExclusionService.retrieveByPlanId(planDetail.getPlanId());
			planDetail.setPlanFacilitiesDTOs(planFacilities);
			planDetail.setPlanExclusionDTOs(exclusions);
		});
		return planDetailsDTOs;

	}

	
}
