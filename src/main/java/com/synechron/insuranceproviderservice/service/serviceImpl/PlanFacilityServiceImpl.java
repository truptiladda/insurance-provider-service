package com.synechron.insuranceproviderservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.insuranceproviderservice.dto.PlanFacilitiesDTO;
import com.synechron.insuranceproviderservice.entity.PlanFacilities;
import com.synechron.insuranceproviderservice.mapper.PlanFacilityMapper;
import com.synechron.insuranceproviderservice.repository.PlanFacilityRepository;
import com.synechron.insuranceproviderservice.service.PlanFacilityService;

@Service
public class PlanFacilityServiceImpl implements PlanFacilityService {

	@Autowired
	private PlanFacilityRepository planFacilityRepository;

	@Autowired
	private PlanFacilityMapper planFacilityMapper;

	@Override
	public List<PlanFacilitiesDTO> retrieveByPlanId(Integer planId) {
		List<PlanFacilities> planFacilities = planFacilityRepository.findByPlanId(planId);
		return planFacilityMapper.convertEntityListToDTOList(planFacilities);
	}

	@Override
	public List<PlanFacilitiesDTO> saveAll(List<PlanFacilitiesDTO> planFacilitiesDTOs) {
		List<PlanFacilities> planFacilities = planFacilityMapper.convertDTOListToEntityList(planFacilitiesDTOs);
		List<PlanFacilities> saved = planFacilityRepository.saveAll(planFacilities);
		return planFacilityMapper.convertEntityListToDTOList(saved);
	}

	@Override
	public List<PlanFacilitiesDTO> updateAll(List<PlanFacilitiesDTO> planFacilitiesDTOs) {
		List<PlanFacilitiesDTO> updated = new ArrayList<>();
		planFacilitiesDTOs.forEach(planFacilityDTO -> {
			PlanFacilities planFaciity = planFacilityRepository.findById(planFacilityDTO.getPlanFacilityId()).get();
			if (planFaciity != null) {
				planFaciity = planFacilityMapper.convertDTOToEntity(planFacilityDTO);
				PlanFacilities saved = planFacilityRepository.save(planFaciity);
				updated.add(planFacilityMapper.convertEntityToDTO(saved));

			}
		});
		return updated;
	}

	@Override
	public void deleteByPlanId(Integer planId) {
		List<PlanFacilities> planFacilities = planFacilityRepository.findByPlanId(planId);
		planFacilities.forEach(planFacility -> {
			planFacilityRepository.delete(planFacility);
		});
	}

}
