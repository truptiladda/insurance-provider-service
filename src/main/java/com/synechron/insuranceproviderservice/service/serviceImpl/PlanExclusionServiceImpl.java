package com.synechron.insuranceproviderservice.service.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.synechron.insuranceproviderservice.dto.PlanExclusionsDTO;
import com.synechron.insuranceproviderservice.entity.PlanExclusions;
import com.synechron.insuranceproviderservice.mapper.PlanExclusionsMapper;
import com.synechron.insuranceproviderservice.repository.PlanExclusionRepository;
import com.synechron.insuranceproviderservice.service.PlanExclusionService;

@Service
public class PlanExclusionServiceImpl implements PlanExclusionService {

	@Autowired
	private PlanExclusionRepository planExclusionRepository;

	@Autowired
	private PlanExclusionsMapper planExclusionMapper;

	@Override
	public List<PlanExclusionsDTO> retrieveByPlanId(Integer planId) {
		List<PlanExclusions> planExclusions = planExclusionRepository.findByPlanId(planId);
		return planExclusionMapper.convertEntityListToDTOList(planExclusions);
	}

	@Override
	public List<PlanExclusionsDTO> saveAll(List<PlanExclusionsDTO> planExclusionsDTOs) {
		List<PlanExclusions> planExclusions = planExclusionMapper.convertDTOListToEntityList(planExclusionsDTOs);
		List<PlanExclusions> saved = planExclusionRepository.saveAll(planExclusions);
		return planExclusionMapper.convertEntityListToDTOList(saved);
	}

	@Override
	public List<PlanExclusionsDTO> updateAll(List<PlanExclusionsDTO> planExclusionsDTOs) {
		List<PlanExclusionsDTO> updated = new ArrayList<>();
		planExclusionsDTOs.forEach(planExclusionDTO -> {
			PlanExclusions planExclusion = planExclusionRepository.findById(planExclusionDTO.getPlanExclusionId())
					.get();
			if (planExclusion != null) {
				planExclusion = planExclusionMapper.convertDTOToEntity(planExclusionDTO);
				PlanExclusions saved = planExclusionRepository.save(planExclusion);
				updated.add(planExclusionMapper.convertEntityToDTO(saved));

			}
		});
		return updated;
	}

	@Override
	public void deleteByPlanId(Integer planId) {
		List<PlanExclusions> planExclusions = planExclusionRepository.findByPlanId(planId);
		planExclusions.forEach(exclusion -> {
			planExclusionRepository.delete(exclusion);
		});
	}

}
