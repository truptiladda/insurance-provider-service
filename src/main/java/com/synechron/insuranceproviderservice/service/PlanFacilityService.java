package com.synechron.insuranceproviderservice.service;

import java.util.List;

import com.synechron.insuranceproviderservice.dto.PlanFacilitiesDTO;

public interface PlanFacilityService {
	List<PlanFacilitiesDTO> retrieveByPlanId(Integer planId);

	List<PlanFacilitiesDTO> saveAll(List<PlanFacilitiesDTO> planFacilitiesDTOs);

	List<PlanFacilitiesDTO> updateAll(List<PlanFacilitiesDTO> planFacilitiesDTOs);

	void deleteByPlanId(Integer planId);
}
