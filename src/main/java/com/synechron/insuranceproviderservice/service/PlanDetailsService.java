package com.synechron.insuranceproviderservice.service;

import java.util.List;

import com.synechron.insuranceproviderservice.dto.PlanDetailsDTO;

public interface PlanDetailsService {

	PlanDetailsDTO updatePlan(PlanDetailsDTO planDetailsDTO);

	List<PlanDetailsDTO> retrieveAllPlans();

	PlanDetailsDTO retrieveByPlanId(Integer planId);

	void deleteByPlanId(Integer planId);

	List<PlanDetailsDTO> updateAllPlans(List<PlanDetailsDTO> planDetailsDTOs);

	List<PlanDetailsDTO> retrieveByPlanType(Integer planTypeId);

	PlanDetailsDTO createPlan(PlanDetailsDTO planDetailsDTO);

	List<PlanDetailsDTO> createAllPlans(List<PlanDetailsDTO> planDetailsDTOs);

}
