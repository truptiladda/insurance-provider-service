package com.synechron.insuranceproviderservice.enums;

public enum InsuranceTypes {

	TEAM_INSURANCE("teamInsurance"), HEALTH_INSURANCE("healthInsurance"), INVESTMENT("investment"), CANCER("cancer"),
	CHILD("child"), RETIREMENT("retirement"), CAR("car"), BIKE("bike");

	InsuranceTypes(String string) {
		// TODO Auto-generated constructor stub
	}

	private String insuranceType;

	public String getInsuranceType() {
		return insuranceType;
	}

}
