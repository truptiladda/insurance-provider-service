package com.synechron.insuranceproviderservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.synechron.insuranceproviderservice.entity.PlanExclusions;

@Repository
public interface PlanExclusionRepository extends JpaRepository<PlanExclusions, Integer> {

	List<PlanExclusions> findByPlanId(@Param("planId") Integer planId);
}
