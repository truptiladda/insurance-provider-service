package com.synechron.insuranceproviderservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.synechron.insuranceproviderservice.entity.PlanFacilities;

@Repository
public interface PlanFacilityRepository extends JpaRepository<PlanFacilities, Integer> {

	List<PlanFacilities> findByPlanId(@Param("planId") Integer planId);
}
