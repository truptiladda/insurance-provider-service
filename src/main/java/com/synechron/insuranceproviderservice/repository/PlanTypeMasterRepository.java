package com.synechron.insuranceproviderservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.synechron.insuranceproviderservice.entity.PlanTypeMaster;
@Repository
public interface PlanTypeMasterRepository extends JpaRepository<PlanTypeMaster, Integer> {

}
