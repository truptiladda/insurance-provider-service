package com.synechron.insuranceproviderservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.synechron.insuranceproviderservice.entity.PlanDetails;

@Repository
public interface PlanDetailsRepository extends JpaRepository<PlanDetails, Integer> {

	List<PlanDetails> findByPlanTypeId(@Param("planTypeId") Integer planTypeId);
}
