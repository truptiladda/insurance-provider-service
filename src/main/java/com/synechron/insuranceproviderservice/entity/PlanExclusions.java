package com.synechron.insuranceproviderservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "plan_exclusions", schema = "[plan]")
@Data
@NamedQueries({
		@NamedQuery(name = "PlanExclusions.findByPlanId", query = "SELECT p FROM PlanExclusions p where p.planId.planId = :planId") })
public class PlanExclusions implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plan_exclusions_id")
	private Integer planExclusionId;

	@Column(name = "plan_exclusion")
	private String planExclusion;

	@JoinColumn(name = "plan_id", referencedColumnName = "plan_id")
	@ManyToOne
	private PlanDetails planId;

}
