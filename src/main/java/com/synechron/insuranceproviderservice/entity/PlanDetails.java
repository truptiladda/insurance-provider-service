package com.synechron.insuranceproviderservice.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "plan_details", schema = "[plan]")
@Data
@NamedQueries({
	@NamedQuery(name = "PlanDetails.findByPlanTypeId", query = "SELECT p FROM PlanDetails p where p.planTypeId.planTypeId = :planTypeId") })
public class PlanDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plan_id")
	private Integer planId;

	@Column(name = "plan_name")
	private String planName;

	@Column(name = "entry_age")
	private Integer entryAge;

	@Column(name = "maturity_age")
	private Integer maturityAge;

	@Column(name = "premium_yearly")
	private BigDecimal premiumYearly;

	@Column(name = "is_copayment")
	private Boolean isCopayment;

	@JoinColumn(name = "plan_type_id", referencedColumnName = "plan_type_id")
	@ManyToOne
	private PlanTypeMaster planTypeId;

}
