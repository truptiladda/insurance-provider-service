package com.synechron.insuranceproviderservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "plan_type_master",schema= "[plan]")
@Data
public class PlanTypeMaster implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plan_type_id")
	private Integer planTypeId;

	@Column(name = "plan_type_name")
	private String planTypeName;

}
