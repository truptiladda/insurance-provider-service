package com.synechron.insuranceproviderservice.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "plan_facilities",schema = "[plan]")
@Data
@NamedQueries({
		@NamedQuery(name = "PlanFacilities.findByPlanId", query = "SELECT p FROM PlanFacilities p where p.planId.planId = :planId") })
public class PlanFacilities implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plan_facility_id")
	private Integer planFacilityId;

	@Column(name = "plan_facility")
	private String planFacility;

	@JoinColumn(name = "plan_id", referencedColumnName = "plan_id")
	@ManyToOne
	private PlanDetails planId;
}
