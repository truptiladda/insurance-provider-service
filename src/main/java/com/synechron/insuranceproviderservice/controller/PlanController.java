package com.synechron.insuranceproviderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.insuranceproviderservice.dto.PlanDetailsDTO;
import com.synechron.insuranceproviderservice.dto.ResponseDTO;
import com.synechron.insuranceproviderservice.service.PlanDetailsService;

@CrossOrigin
@RestController
@RequestMapping("insuranceProvider/api/v1")
public class PlanController {

	@Autowired
	private PlanDetailsService planDetailsService;

	/*
	 * @GetMapping("/plans/{input}") public ResponseDTO testController(Integer
	 * input) { ResponseDTO responseDTO =
	 * ResponseDTO.builder().isSuccess(true).data(input).statusCode(200).message(
	 * "success") .build(); return responseDTO; }
	 */

	@PostMapping("/plans")
	public ResponseDTO createAllPlans(@RequestBody List<PlanDetailsDTO> planDetailsDTOs) {

		List<PlanDetailsDTO> planDetails = planDetailsService.createAllPlans(planDetailsDTOs);
		if (!CollectionUtils.isEmpty(planDetails)) {
			return ResponseDTO.builder().isSuccess(true).message("Success").statusCode(200).data(planDetails).build();
		} else {
			return ResponseDTO.builder().isSuccess(false).message("Bad Requst").statusCode(200).build();
		}
	}

	@PutMapping("/plans")
	public ResponseDTO updateAllPlans(@RequestBody List<PlanDetailsDTO> planDetailsDTOs) {
		List<PlanDetailsDTO> planDetails = planDetailsService.updateAllPlans(planDetailsDTOs);
		if (!CollectionUtils.isEmpty(planDetails)) {
			return ResponseDTO.builder().isSuccess(true).message("Success").statusCode(200).data(planDetails).build();
		} else {
			return ResponseDTO.builder().isSuccess(false).message("Bad Requst").statusCode(200).build();
		}
	}

	@GetMapping("/plans/{planId}")
	public ResponseDTO getPlan(@PathVariable Integer planId) {
		PlanDetailsDTO planDetailsDTO = planDetailsService.retrieveByPlanId(planId);
		if (planDetailsDTO != null) {
			return ResponseDTO.builder().isSuccess(true).message("Success").statusCode(200).data(planDetailsDTO)
					.build();
		} else {
			return ResponseDTO.builder().isSuccess(false).message("No Record Found").statusCode(404).build();
		}

	}

	@DeleteMapping("/plans/{planId}")
	public ResponseDTO deletePlan(@PathVariable Integer planId) {
		planDetailsService.deleteByPlanId(planId);
		return ResponseDTO.builder().isSuccess(true).message("Success").statusCode(200).build();
	}

	@GetMapping("plans/planType/{planTypeId}")
	public ResponseDTO retrieveByPlanTypeId(@PathVariable Integer planTypeId) {
		List<PlanDetailsDTO> planDetailsDTOs = planDetailsService.retrieveByPlanType(planTypeId);
		if (!CollectionUtils.isEmpty(planDetailsDTOs)) {
			return ResponseDTO.builder().isSuccess(true).message("Success").statusCode(200).data(planDetailsDTOs)
					.build();
		} else {
			return ResponseDTO.builder().isSuccess(false).message("No Record found").statusCode(404).build();
		}
	}

}
