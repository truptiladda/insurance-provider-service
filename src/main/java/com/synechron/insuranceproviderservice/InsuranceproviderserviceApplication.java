package com.synechron.insuranceproviderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceproviderserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceproviderserviceApplication.class, args);
	}

}
